/*
 * A simple Matrix text rain effect.
 *
 * Author: Matti Kuonanoja (2015)
 */

#include <c64.h>
#include <conio.h>
#include "streameffect.h"
#include "fps.h"

void print_fps();

int main(void) {
    char key;
    char show_fps = 0;
    char quit = 0;

    stream_effect_init();

    while (!quit) {
        fps_frame_started();
        stream_effect_update();
        if (kbhit()){
            key = cgetc();
            if (key == 32)
                show_fps = !show_fps;
            else
                quit = 1;
        }
        if (show_fps) {
            print_fps();
        }
    }
    stream_effect_restore_screen();

    return 0;
}

void print_fps() {
    gotoxy(0, 0);
    textcolor(COLOR_WHITE);
    cprintf("fps [%4i]    ", fps_get_average());
}
