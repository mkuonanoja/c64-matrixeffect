/*
 * Author: Matti Kuonanoja (2015)
 */

unsigned int fps_get_average();

void fps_frame_started();
