/*
 * Author: Matti Kuonanoja (2015)
 */

#include <time.h>
#include "fps.h"

#define SAMPLE_COUNT 32

static unsigned int average_fps = 0;

void fps_frame_started() {
    static unsigned long time_passed;
    static unsigned long previous_system_clock;
    static unsigned long system_clock = 0;
    static unsigned char sample_counter = 0;

    ++sample_counter;
    if (sample_counter < SAMPLE_COUNT)
        return;

    sample_counter = 0;
    system_clock = clock();
    time_passed = system_clock - previous_system_clock;
    previous_system_clock = system_clock;
    average_fps = (CLOCKS_PER_SEC * SAMPLE_COUNT) / time_passed;
}

unsigned int fps_get_average() {
    return average_fps;
}
