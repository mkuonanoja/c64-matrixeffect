/*
 * Author: Matti Kuonanoja (2015)
 */

#ifndef __DATA_STREAMS_H
#define __DATA_STREAMS_H

#define STREAM_COUNT 25

#define STREAM_LENGTH_MIN 4
#define STREAM_LENGTH_MAX 14

#define STREAM_DELAY_MIN 0
#define STREAM_DELAY_MAX 5

#define HEAD_LENGTH 2
#define BODY_LENGTH 2

#define CHARSET_START 64
#define CHARSET_END 127

#define MAX_EMPTY_COLUMN_SEARCH_TRIES 4

void stream_effect_init(void);
void stream_effect_update(void);
void stream_effect_restore_screen(void);

#endif // __DATA_STREAMS_H
