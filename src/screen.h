/*
 * Author: Matti Kuonanoja (2015)
 */

#ifndef __SCREEN_H
#define __SCREEN_H

// Sets char to given
void screen_set_char(char x, char y, char c);

// Sets empty char to given screen position
void screen_clear_char(char x, char y);

// Sets char color on given screen position
void screen_set_color(char x, char y, char color);

void screen_set_active_column(char x);
void screen_set_column_char(char y, char c);
void screen_clear_column_char(char y);
void screen_set_column_color(char y, char color);

void screen_enable_upper_charset(char enable);

#endif // __SCREEN_H
